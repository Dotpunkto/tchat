﻿using System;
using System.Collections.Generic;
using Fluxor;
using Microsoft.AspNetCore.SignalR.Client;
using Tchat.Model;

namespace Tchat.Store
{
    public static class Reducers
    {
        [ReducerMethod]
        public static TchatState ReduceConnectionAction(TchatState state, ConnectionAction action) =>
            new TchatState(action.Connection , action.User, action.RoomList, state.CurrentRoom);

        [ReducerMethod]
        public static TchatState ReduceUpdateCurrentRoomAction(TchatState state, ChangeCurrentRoomAction action) =>
            new TchatState(state.Connection, state.User, state.RoomList, action.CurrentRoom);

        [ReducerMethod]
        public static TchatState ReduceDisconnectionAction(TchatState _, DisconnectionAction action) =>
            new TchatState(action.Connection, action.User, action.RoomList, action.CurrentRoom);

        [ReducerMethod]
        public static TchatState ReduceAddRoomAction(TchatState state, AddRoomAction action)
        {
            return new TchatState(state.Connection, state.User, action.RoomList, state.CurrentRoom);
        }
    }
}
