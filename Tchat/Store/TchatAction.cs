﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.SignalR.Client;
using Tchat.Model;

namespace Tchat.Store
{
    public class ConnectionAction
    {
        public HubConnection Connection { get; }
        public User User { get; set; }
        public List<Room> RoomList { get; set; }
        public Room CurrentRoom { get; set; }

        public ConnectionAction(HubConnection connection, User user, List<Room> roomList, Room room)
        {
            Connection = connection;
            User = user;
            RoomList = roomList;
            CurrentRoom = room;
        }
    }

    public class DisconnectionAction
    {
        public HubConnection Connection = null;
        public User User = null;
        public List<Room> RoomList = null;
        public Room CurrentRoom = null;
    }

    public class ChangeCurrentRoomAction
    {
        public Room CurrentRoom { get; set; }

        public ChangeCurrentRoomAction(Room room)
        {
            CurrentRoom = room;
        }
    }

    public class AddRoomAction
    {
        public List<Room> RoomList { get; set; }

        public AddRoomAction(List<Room> roomList)
        {
            RoomList = roomList;
        }
    }
}
