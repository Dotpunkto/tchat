﻿using System;
using System.Collections.Generic;
using Microsoft.AspNetCore.SignalR.Client;
using Tchat.Model;

namespace Tchat.Store
{
    public class TchatState
    {
        public HubConnection Connection { get; }
        public User User { get; }
        public List<Room> RoomList { get; }
        public Room CurrentRoom { get; }

        public TchatState(HubConnection connection, User user, List<Room> roomList, Room room)
        {
            Connection = connection;
            User = user;
            RoomList = roomList;
            CurrentRoom = room;
        }
    }
}
