﻿using System;
using Fluxor;

namespace Tchat.Store
{
    public class Feature : Feature<TchatState>
    {
        public override string GetName() => "Connection";
        protected override TchatState GetInitialState() =>
            new TchatState(connection: null, user: null,  roomList: null,  room: null);
    }
}
