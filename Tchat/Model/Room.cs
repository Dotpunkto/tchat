﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tchat.Model
{
    public class Room
    {
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public int UserID { get; set; }
    }
}
