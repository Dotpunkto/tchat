﻿using System;
using System.Collections.Generic;

namespace Tchat.Model
{
    public class LoginResponse
    {
        public int UserID { get; set; }
        public List<Room> List { get; set; }
    }
}
