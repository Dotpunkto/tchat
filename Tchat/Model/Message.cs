﻿using System;
using System.ComponentModel.DataAnnotations;

namespace Tchat.Model
{
    public class Message
    {
        [Required]
        public string Content { get; set; }
        [Required]
        public DateTime Date = DateTime.Now;
        [Required]
        public int UserID { get; set; }
        public User User { get; set; }
        [Required]
        public int RoomID { get; set; }
    }
}
