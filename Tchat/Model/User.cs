﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace Tchat.Model
{
    public class User
    {
        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Password { get; set; }
    }
}
