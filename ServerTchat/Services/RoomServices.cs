﻿using System.Collections.Generic;
using ServerTchat.Entities;
using ServerTchat.Interface;
using Tchat.Model;

namespace ServerTchat.Services
{
    public class RoomServices: IRoomServices
    {
        private readonly IRoomRepository RoomRepository;
        private readonly IUsersServices UsersServices;

        public RoomServices(IRoomRepository roomRepository, IUsersServices usersServices)
        {
            RoomRepository = roomRepository;
            UsersServices = usersServices;
        }

        public int NewRoom(Room room)
        {
            Room existingRoom = RoomRepository.GetRoomByName(room.Name);

            if (existingRoom == null)
            {
                User user = UsersServices.FindUserByID(room.UserID);
                Room newRoom = CreateRoom(user, room.Name);

                return RoomRepository.CreateRoom(newRoom);
            }
            return -1;
        }

        private Room CreateRoom(User user, string name)
        {
            Room room = new Room()
            {
                Name = name,
                UserID = user.ID,
            };
            room.Users.Add(user);

            return room;
        }

        public bool JoinRoom(RoomUser roomUser)
        {
            var existingRoom = RoomRepository.GetRoomByID(roomUser.RoomID);
            var existingUser = UsersServices.FindUserByID(roomUser.UserID);
            if (!existingRoom.Users.Contains(existingUser))
            {
                existingRoom.Users.Add(existingUser);
                RoomRepository.UpdateRoomUser(existingRoom);
                return true;
            }
            return false;
        }

        public List<Room> GetAllRooms()
        {
            return RoomRepository.GetAllRooms();
        }
    }
}
