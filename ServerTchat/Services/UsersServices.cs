﻿using ServerTchat.Entities;
using ServerTchat.Interface;

namespace ServerTchat.Services
{
    public class UsersServices: IUsersServices
    {
        private readonly IUserRepository UserRepository;

        public UsersServices(IUserRepository userRepository)
        {
            UserRepository = userRepository;
        }

        public User FindUserByID(int ID)
        {
            return UserRepository.FindUserByID(ID);
        }

        public User Login(User user)
        {
            return UserRepository.Login(user);
        }

        public User Register(User user)
        {
            return UserRepository.Register(user);
        }

        public void AddUser(User user)
        {
            UserRepository.AddUser(user);
        }
    }
}
