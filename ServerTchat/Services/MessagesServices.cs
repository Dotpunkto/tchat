﻿using System.Linq;
using ServerTchat.Entities;
using ServerTchat.Interface;

namespace ServerTchat.Services
{
    public class MessagesServices : IMessagesServices
    {
        private readonly IMessageRepository MessageRepository;

        public MessagesServices(IMessageRepository messageRepository)
        {
            MessageRepository = messageRepository;
        }

        public IQueryable<Message> GetRoomMessage(int roomID)
        {
            return MessageRepository.GetRoomMessage(roomID);
        }

        public void SaveMessage(Message message)
        {
            MessageRepository.SaveMessage(message);
        }
    }
}
