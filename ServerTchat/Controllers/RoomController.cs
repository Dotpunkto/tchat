﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ServerTchat.Entities;
using ServerTchat.Interface;
using Tchat.Model;

namespace ServerTchat.Controllers
{
    [Route("[controller]/[action]")]
    public class RoomController : Controller
    {
        private readonly IRoomServices RoomServices;

        public RoomController(IRoomServices roomServices)
        {
            RoomServices = roomServices;
        }

        [HttpPost]
        public IActionResult RoomPost([FromBody] Room room)
        {
            if (ModelState.IsValid)
            {
                int ID = RoomServices.NewRoom(room);
                if (ID == -1)
                {
                    return BadRequest();
                }
                return Ok(new { ID = ID });
            }
            return BadRequest();
        }

        [HttpPost]
        public IActionResult JoinRoom([FromBody] RoomUser roomUser)
        {
            if (ModelState.IsValid && RoomServices.JoinRoom(roomUser))
            {
                return Ok();
            }
            return BadRequest();
        }

        [HttpGet]
        public List<Room> GetRooms()
        {
            return RoomServices.GetAllRooms();
        }
    }
}