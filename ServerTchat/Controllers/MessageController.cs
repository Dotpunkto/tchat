﻿using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using ServerTchat.Entities;
using ServerTchat.Interface;

namespace ServerTchat.Controllers
{
    [ApiController]
    [Route("[controller]/[action]")]
    public class MessageController : Controller
    {
        private readonly IMessagesServices MessagesServices;

        public MessageController(IMessagesServices messagesServices)
        {
            MessagesServices = messagesServices;
        }

        [HttpGet]
        public IEnumerable<Message> GetMessages([FromQuery] int roomID)
        {
            return MessagesServices.GetRoomMessage(roomID);
        }
    }
}