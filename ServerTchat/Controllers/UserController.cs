﻿using Microsoft.AspNetCore.Mvc;
using ServerTchat.Entities;
using ServerTchat.Interface;

namespace ServerTchat.Controllers
{
    [Route("[controller]/[action]")]
    public class UserController : Controller
    {
        private readonly IUsersServices UsersServices;

        public UserController(IUsersServices usersServices)
        {
            UsersServices = usersServices;
        }

        [HttpPost]
        public IActionResult LogInPost([FromBody] User user)
        {
            User existingUser = UsersServices.Login(user);

            if (existingUser != null)
            {
                return Ok(new { UserID = existingUser.ID, List = existingUser.Rooms });
            }

            return BadRequest();
        }

        [HttpPost]
        public IActionResult RegisterPost([FromBody] User user)
        {
            User existingUser = UsersServices.Register(user);

            if (existingUser == null)
            {
                UsersServices.AddUser(user);
                return Ok();
            }
            return BadRequest();
        }

    }
}
