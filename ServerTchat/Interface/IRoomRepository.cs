﻿using System.Collections.Generic;
using ServerTchat.Entities;

namespace ServerTchat.Interface
{
    public interface IRoomRepository
    {
        Room GetRoomByName(string name);
        int CreateRoom(Room room);
        List<Room> GetAllRooms();
        Room GetRoomByID(int ID);
        void UpdateRoomUser(Room room);
    }
}
