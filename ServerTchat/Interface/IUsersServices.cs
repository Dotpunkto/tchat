﻿using ServerTchat.Entities;

namespace ServerTchat.Interface
{
    public interface IUsersServices
    {
        User FindUserByID(int ID);
        User Login(User user);
        User Register(User user);
        void AddUser(User user);
    }
}
