﻿using System.Linq;
using ServerTchat.Entities;

namespace ServerTchat.Interface
{
    public interface IMessageRepository
    {
        IQueryable<Message> GetRoomMessage(int roomID);
        void SaveMessage(Message message);
    }
}
