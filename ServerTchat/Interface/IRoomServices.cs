﻿using System.Collections.Generic;
using ServerTchat.Entities;
using Tchat.Model;

namespace ServerTchat.Interface
{
    public interface IRoomServices
    {
        int NewRoom(Room room);
        List<Room> GetAllRooms();
        bool JoinRoom(RoomUser roomUser);
    }
}
