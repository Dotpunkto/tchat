﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace ServerTchat.Entities
{
    public class Room
    {
        public Room()
        {
            Users = new HashSet<User>();
        }

        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public int UserID { get; set; }
        [JsonIgnore]
        public virtual ICollection<User> Users { get; set; }

    }
}
