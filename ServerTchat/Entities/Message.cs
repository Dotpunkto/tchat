﻿using System;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace ServerTchat.Entities
{
    public class Message
    {
        public int ID { get; set; }
        [Required]
        public string Content { get; set; }
        [Required]
        public DateTime Date { get; set; }
        [Required]
        public int UserID { get; set; }
        public User User { get; set; }
        [Required]
        public int RoomID { get; set; }
        [JsonIgnore]
        public Room Room { get; set; }
    }
}
