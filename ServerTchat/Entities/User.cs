﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Text.Json.Serialization;

namespace ServerTchat.Entities
{
    public class User
    {
        public User()
        {
            Rooms = new HashSet<Room>();
        }

        public int ID { get; set; }
        [Required]
        public string Name { get; set; }
        [Required]
        public string Password { get; set; }

        public virtual ICollection<Room> Rooms { get; set; }
        [JsonIgnore]
        public List<Message> Messages { get; set; }
    }
}
