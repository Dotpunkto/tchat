﻿using System;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using ServerTchat.Entities;
using ServerTchat.Interface;

namespace ServerTchat.TchatHub
{
    public class TchatBlaBla : Hub
    {
        private readonly IMessagesServices MessagesServices;
        private readonly IUsersServices UsersServices;

        public TchatBlaBla(IMessagesServices messagesServices, IUsersServices usersServices)
        {
            MessagesServices = messagesServices;
            UsersServices = usersServices;
        }

        public override Task OnConnectedAsync()
        {
            Console.WriteLine($"{Context.ConnectionId} connected");
            return base.OnConnectedAsync();
        }

        public override async Task OnDisconnectedAsync(Exception e)
        {
            Console.WriteLine($"Disconnected {e?.Message} {Context.ConnectionId}");
            await base.OnDisconnectedAsync(e);
        }

        public async Task SendMessage(Message message)
        {
            MessagesServices.SaveMessage(message);
            message.User = UsersServices.FindUserByID(message.UserID);
            await Clients.All.SendAsync($"{message.RoomID}", message);
        }

    }
}
