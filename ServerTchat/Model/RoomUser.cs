﻿

namespace Tchat.Model
{
    public class RoomUser
    {
        public int RoomID { get; set; }
        public int UserID { get; set; }
    }
}
