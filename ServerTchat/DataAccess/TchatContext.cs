﻿using System;
using Microsoft.EntityFrameworkCore;
using ServerTchat.Entities;

namespace ServerTchat.DataAccess
{
    public class TchatContext : DbContext
    {
        public TchatContext(DbContextOptions<TchatContext> options)
            : base(options)
        {
        }

        public DbSet<User> User { get; set; }
        public DbSet<Room> Room { get; set; }
        public DbSet<Message> Message { get; set; }
    }
}
