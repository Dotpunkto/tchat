using Fluxor;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using ServerTchat.Controllers;
using ServerTchat.DataAccess;
using ServerTchat.Interface;
using ServerTchat.Repository;
using ServerTchat.Services;
using ServerTchat.TchatHub;

namespace ServerTchat
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors(options =>
            {
                options.AddPolicy("CorsPolicy", policy =>
                {
                    policy.AllowAnyOrigin().AllowAnyMethod().AllowAnyHeader();
                });
            });
            services.AddSignalR();
            services.AddControllers();
            var currentAssembly = typeof(Startup).Assembly;
            services.AddFluxor(options => options.ScanAssemblies(currentAssembly));
            services.AddDbContext<TchatContext>(options =>
                options.UseSqlite(Configuration.GetConnectionString("TchatContext")));

            services.AddScoped<IMessagesServices, MessagesServices>();
            services.AddScoped<IMessageRepository, MessageRepository>();

            services.AddScoped<IUsersServices, UsersServices>();
            services.AddScoped<IUserRepository, UserRepository>();

            services.AddScoped<IRoomServices, RoomServices>();
            services.AddScoped<IRoomRepository, RoomRepository>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseHttpsRedirection();

            app.UseRouting();
            app.UseCors("CorsPolicy");
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapHub<TchatBlaBla>("/TchatHub");
                endpoints.MapControllers();
            });
        }
    }
}