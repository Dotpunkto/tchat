﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using ServerTchat.DataAccess;
using ServerTchat.Entities;
using ServerTchat.Interface;

namespace ServerTchat.Repository
{
    public class MessageRepository: IMessageRepository
    {
        private readonly TchatContext context;

        public MessageRepository(TchatContext context)
        {
            this.context = context;
        }

        public IQueryable<Message> GetRoomMessage(int roomID)
        {
            return context.Message.Include(m => m.User).Where(m => m.RoomID == roomID);
        }

        public void SaveMessage(Message message)
        {
            context.Message.Add(message);
            context.SaveChanges();
        }
    }
}
