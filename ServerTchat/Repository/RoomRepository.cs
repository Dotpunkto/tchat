﻿using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using ServerTchat.DataAccess;
using ServerTchat.Entities;
using ServerTchat.Interface;

namespace ServerTchat.Repository
{
    public class RoomRepository : IRoomRepository
    {
        private readonly TchatContext Context;

        public RoomRepository(TchatContext context)
        {
            Context = context;
        }

        public Room GetRoomByName(string name)
        {
            return Context.Room.FirstOrDefault(r => r.Name == name);
        }

        public int CreateRoom(Room room)
        {
            Context.Room.Add(room);
            Context.SaveChanges();
            return room.ID;
        }

        public Room GetRoomByID(int ID)
        {
            return Context.Room.Include(r => r.Users).FirstOrDefault(r => r.ID == ID);
        }

        public List<Room> GetAllRooms()
        {
            return Context.Room.ToList();
        }

        public void UpdateRoomUser(Room room)
        {
            Context.Room.Update(room);
            Context.SaveChanges();
        }
    }
}
