﻿using System.Linq;
using Microsoft.EntityFrameworkCore;
using ServerTchat.DataAccess;
using ServerTchat.Entities;
using ServerTchat.Interface;

namespace ServerTchat.Repository
{
    public class UserRepository : IUserRepository
    {
        private readonly TchatContext Context;

        public UserRepository(TchatContext context)
        {
            Context = context;
        }

        public User FindUserByID(int ID)
        {
            return Context.User.Find(ID);
        }

        public User Login(User user)
        {
            return Context.User.Include(u => u.Rooms).FirstOrDefault(u => u.Name == user.Name && u.Password == user.Password);
        }

        public User Register(User user)
        {
            return Context.User.FirstOrDefault(u => u.Name == user.Name && u.Password == user.Password);
        }

        public void AddUser(User user)
        {
            Context.User.Add(user);
            Context.SaveChanges();
        }
    }
}
